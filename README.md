# CleanDaedalusScriptsPL

Scripts without singleplayer stuff, this should be the base for your addons. Highly experimental, probably will change a lot...

## What's changed
- Removed dialogues, quests, AI, quest items, all NPCs (Only Monsters and PC_HERO exists), essentially all singleplayer stuff
- Some instances were changed so the naming convention has better consistency
- Item interaction - While using an item like HP potion only animation will play, you have to handle stuff (restoring hp, mana etc) like that in squirrel side
